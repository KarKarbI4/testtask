﻿using System;

public class Triangle
{

    /// <exception cref="ArgumentOutOfRangeException">Thrown if one of arguments is less or equal zero</exception>
    /// <exception cref="ArgumentException">Thrown if arguments are not sides of a right triangle</exception>

    public const string argBelowZeroMessage = "Sides must be positive";
    public const string notRightTriangle = "Arguments are not sides of a right triangle";

    public static double getSquare(double a, double b, double c)
    {
        if (a <= 0 || b <= 0 || c <= 0)
        {
            throw new ArgumentOutOfRangeException("Sides must be positive");
        }
        if (c < a)
        {
            double temp = c;
            c = a;
            a = temp;
        }
        if (c < b)
        {
            double temp = c;
            c = b;
            b = temp;
        }
        if (Math.Pow(c, 2) != Math.Pow(a, 2) + Math.Pow(b, 2))
        {
            throw new ArgumentException("Arguments are not sides of a right triangle");
        }
        return a * b / 2f;
    }
}
