﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TriangleTest
{
    
    [TestClass]
    public class UnitTest
    {
        
        [TestMethod]
        public void Test_ValidArgs_Answer()
        {
            double a = 3;
            double b = 4;
            double c = 5;
            double expected = 6;

            double actual = Triangle.getSquare(a, b, c);

            Assert.AreEqual(expected, actual, 0.001, "Incorrect calculated square");
        }

        [TestMethod]
        public void Test_ArgLEzero() {
            double a = -3;
            double b = 4;
            double c = 5;

            try {
                double square = Triangle.getSquare(a, b, c);
            } catch (ArgumentOutOfRangeException e) {
                StringAssert.Contains(e.Message, Triangle.argBelowZeroMessage);
                return;
            }
            Assert.Fail("No exception was thrown.");
        }

        [TestMethod]
        public void Test_NotRightTriangle()
        {
            double a = 2;
            double b = 4;
            double c = 5;

            try
            {
                double square = Triangle.getSquare(a, b, c);
            }
            catch (ArgumentException e)
            {
                StringAssert.Contains(e.Message, Triangle.notRightTriangle);
                return;
            }
            Assert.Fail("No exception was thrown.");
        }
    }
}
